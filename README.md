Panoptic Segmentation - Reviews and Summaries
==========================================================

This repository contains summaries of interesting papers (mostly related to panoptic segmentation).
See [Issues](https://git.rwth-aachen.de/mark.weber/readings/issues)

Discussed Papers
----------------------------------------------------------

### Panoptic Segmentation

* [UPSNet: A Unified Panoptic Segmentation Network](https://git.rwth-aachen.de/mark.weber/readings/issues/9)
* [Weakly- and Semi-Supervised Panoptic Segmentation](https://git.rwth-aachen.de/mark.weber/readings/issues/8)
* [Panoptic Feature Pyramid Networks](https://git.rwth-aachen.de/mark.weber/readings/issues/7)
* [Attention-guided Unified Network for Panoptic Segmentation](https://git.rwth-aachen.de/mark.weber/readings/issues/6)
* [Learning to Fuse Things and Stuff](https://git.rwth-aachen.de/mark.weber/readings/issues/1)

### Multi-task Learning

* [Multi-Task Learning Using Uncertainty to Weigh Losses for Scene Geometry and Semantics](https://git.rwth-aachen.de/mark.weber/readings/issues/2)
* [Fast Scene Understanding for Autonomous Driving](https://git.rwth-aachen.de/mark.weber/readings/issues/4)
* [Pixel-level Encoding and Depth Layering for Instance-level Semantic Labeling](https://git.rwth-aachen.de/mark.weber/readings/issues/5)

### Backbones

* [Feature Pyramid Networks for Object Detection](https://git.rwth-aachen.de/mark.weber/readings/issues/10)
* [Deep Residual Learning for Image Recognition](https://git.rwth-aachen.de/mark.weber/readings/issues/3) 


Contact
-----------------------------------------------------------
Mark Weber
work@weber-sindorf.de